import { defineStore } from 'pinia'

const packs = import.meta.glob('../assets/wordpacks/*.json', { eager: true })

export const useVocabularyStore = defineStore({
  id: 'vocabulary',
  state: () => ({
    wordpacks: packs,
    words: [],
    results: [],
    resultVisible: false,
    seed: 0
  }),
  getters: {
  },
  actions: {
  }
})
